Rails.application.routes.draw do
  root 'static_pages#home'
  get 'about'     => 'static_pages#about'
  get 'skills'    => 'static_pages#skills'
  get 'classes'   => 'static_pages#classes'
  match '/contact', to: 'contacts#new', via: 'get'
  resources "contacts", only: [:new, :create]
end
