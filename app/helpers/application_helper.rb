module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Koifrog"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
  def icon_link(icon='', link='')
	  link_to ('<i class="'+icon+'"></i>').html_safe, link
  end
end
