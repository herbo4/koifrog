class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def skills
  end

  def classes
  end

  def contact
  end
end
