require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase

  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Koifrog"
  end

  test "should get about" do
    get :about
    assert_select "title", "About | Koifrog"
    assert_response :success
  end

  test "should get skills" do
    get :skills
    assert_select "title", "Skills | Koifrog"
    assert_response :success
  end

  test "should get classes" do
    get :classes
    assert_select "title", "Classes | Koifrog"
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_select "title", "Contact | Koifrog"
    assert_response :success
  end

end
